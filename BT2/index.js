var tinhTB = function () {
  var num1 = document.getElementById("txt_so_1").value * 1;

  var num2 = document.getElementById("txt_so_2").value * 1;

  var num3 = document.getElementById("txt_so_3").value * 1;

  var num4 = document.getElementById("txt_so_4").value * 1;

  var num5 = document.getElementById("txt_so_5").value * 1;

  var soTB = (num1 + num2 + num3 + num4 + num5) / 5;

  console.log(soTB);
  document.getElementById("ket_qua").innerHTML =
    "Giá trị trung bình là:  " + soTB;
};
/**
 *Input: num1, num2, num3, num4, num5
 *
 * Step:
 * - S1: tạo 5 biến lưu giá trị của 5 input
 * - S2: tạo 1 biến lưu giá trị của output
 * - S3: áp dụng cách tính giá trị trung bình
 *
 * Output: soTB
 */
