var tinhTien = function () {
  var soNgayLam = document.getElementById("txt_so_ngay").value * 1;

  var luongMotNgay = 100000;

  var tienLuong = luongMotNgay * soNgayLam;

  console.log(tienLuong);

  document.getElementById("ket_qua").innerHTML =
    "Tiền lương của bạn là: " + tienLuong + "VND";
};

/**
 * Input: soNgayLam
 *
 * Step:
 * - S1: tạo biến lưu input
 * - S2: tạo biến lưu tiền lương 1 ngày là 100000
 * - S3: tạo biến lưu output
 * - S4: áp dụng công thức theo đề bài cho
 *
 * Output: tienLuong
 */
