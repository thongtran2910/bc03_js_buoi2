var tinhToan = function () {
  var soCanTinh = document.getElementById("txt_number").value * 1;

  var tongHaiSo = Math.floor(soCanTinh / 10) + (soCanTinh % 10);

  console.log(tongHaiSo);
  document.getElementById("ket_qua").innerHTML = "Tổng là: " + tongHaiSo;
};
/**
 * Input: soCanTinh
 *
 * Step:
 * - S1: tạo biến lưu giá trị input
 * - S2: tạo biến lưu giá trị output
 * - S3: áp dụng cách lấy số hàng đơn vị và hàng chục để tính tổng cả 2 số
 *
 * Output: tongHaiSo
 */
