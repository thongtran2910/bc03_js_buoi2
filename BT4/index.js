var tinhToan = function () {
  var chieuDaiHCN = document.getElementById("txt_chieu_dai").value * 1;

  var chieuRongHCN = document.getElementById("txt_chieu_rong").value * 1;

  var dienTichHCN = chieuDaiHCN * chieuRongHCN;

  var chuViHCN = (chieuDaiHCN + chieuRongHCN) * 2;

  console.log("Diện tích HCN: ", dienTichHCN, "Chu vi HCN: ", chuViHCN);
  document.getElementById("ket_qua").innerHTML = `<div>
  <p>Diện tích HCN là: ${dienTichHCN}</p>
  <p>Chu vi HCN là: ${chuViHCN}</p></div>`;
};
/**
 * Input:
 * - chieuDaiHCN
 * - chieuRongHCN
 *
 * Step:
 * - S1: tạo 2 biến lưu giá trị của 2 input
 * - S2: tạo 2 biến lưu giá trị của 2 output
 * - S3: áp dụng công thức tính diện tích và chu vi HCN
 *
 * Output:
 * - Diện tích: dienTichHCN
 * - Chu vi: chuViHCN
 */
