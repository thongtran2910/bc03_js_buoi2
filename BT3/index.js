var chuyenTien = function () {
  var soTienUSD = document.getElementById("txt_tien_usd").value * 1;

  var tyGiaUSD = 23500;

  var soTienVND = soTienUSD * tyGiaUSD;

  console.log(soTienVND);
  document.getElementById("ket_qua").innerHTML = soTienVND + " VND";
};
/**
 * Input: soTienUSD
 *
 * Step:
 * - S1: tạo biến lưu giá trị của 1 input
 * - S2: tạo biến lưu giá trị tỷ giá của 1 USD = 23500 VND
 * - S3: tạo biến lưu giá trị của output
 * - S4: áp dụng cách quy đổi từ USD -> VND
 *
 * Output: soTienVND
 */
